#!/usr/bin/env python3

from keps_intermediate import *
from keps_classical_jet import *

class Intermediate_KEps_Jet_Solution(Intermediate_KEps_Solution, Classical_KEps_Jet_Solution):

    plot_prefix = "intermediate-keps-jet"

    eps_min = 1e-20

    a_u = 0.7
    a_k = 0.7
    a_eps = 0.7
    a_eps_hat = 0.7

    def set_sol(self, new_sol):
        u_k_eps = new_sol.reshape(self.number_of_fields(), self.number_of_nodes())
        self.update_relaxation_parameters(u_k_eps)
        a_u = self.a_u
        a_k = self.a_k
        a_eps = self.a_eps
        a_eps_hat = self.a_eps_hat
        u_relaxed = a_u * u_k_eps[0] + (1 - a_u) * self.u()
        k_relaxed = a_k * u_k_eps[1] + (1 - a_k) * self.k()
        eps_relaxed = a_eps * u_k_eps[2] + (1 - a_eps) * self.eps()
        eps_hat_relaxed = a_eps_hat * u_k_eps[3] + (1 - a_eps_hat) * self.eps_hat()
        u_k_eps_relaxed = np.stack((u_relaxed, k_relaxed, eps_relaxed, eps_hat_relaxed))
        u_k_eps_cons = self.ensure_mom_cons(u_k_eps_relaxed)
        success = self.update_change(u_k_eps_cons)
        if success:
            self.advance_timestep()
            self.fields = u_k_eps_cons
        self.update_cached_fields()
        return success
    def update_relaxation_parameters(self, new_sol):
        difference = new_sol - self.fields
        change_new = [1, 1, 1, 1]
        for i in range(self.number_of_fields()):
            change_new[i] = np.dot(difference[i], difference[i])/np.dot(self.fields[i],self.fields[i]) / self.number_of_nodes()
        if self.iteration > 0:
            old_a = np.array([self.a_u, self.a_k, self.a_eps, self.a_eps_hat])
            new_a = old_a
            for i in range(self.number_of_fields()):
                new_a[i] = max(0.001, min(1.0, (self.change_vec[i] / change_new[i])**2))
        self.change_vec = change_new
    def ensure_mom_cons(self, new_sol=None):
        if new_sol is None:
            new_sol_safe = self.fields
        else:
            new_sol_safe = new_sol
        u = new_sol_safe[0]
        k = new_sol_safe[1]
        eps = new_sol_safe[2]
        eps_hat = new_sol_safe[3]
        sqrt_J_over_x = 1 / u[0]
        u_norm = u * sqrt_J_over_x
        k_norm = k * sqrt_J_over_x**2
        eps_norm = eps * sqrt_J_over_x**3
        eps_hat_norm = eps_hat * sqrt_J_over_x**3
        if new_sol is None:
            self.fields[0] = u_norm
            self.fields[1] = k_norm
            self.fields[2] = eps_norm
            self.fields[3] = eps_hat_norm
        return np.stack((u_norm, k_norm, eps_norm, eps_hat_norm))
    # boundary conditions
    def eps_hat_bc_left(self):
        return self.eps_hat()[1]
    def eps_hat_bc_right(self):
        return self.eps_hat_min
    # equations
    def k_convection(self, sol_new):
            return self.u_inner() * self.n_inner() * sol_new.k_inner()
    def k_diffusion(self, sol_new):
        return 1/self.sigma_k * sol_new.k_diff2()
    def eps_convection(self, sol_new):
            return 5/2 * self.u_inner() * self.n_inner() * sol_new.eps_inner()
    def eps_diffusion(self, sol_new):
        return 1/self.sigma_eps * sol_new.eps_diff2()
    def eps_hat_convection(self, sol_new):
            return 5/2 * self.u_inner() * self.n_inner() * sol_new.eps_hat_inner()
    def eps_hat_diffusion(self, sol_new):
        return 1/self.sigma_eps_hat * sol_new.eps_hat_diff2()
    def k_equation(self, sol_new):
        return self.k_diffusion(sol_new)\
            - self.v_inner() * sol_new.k_diff1() \
            + self.k_convection(sol_new) \
            + (self.eps_hat_inner() / self.eps_lim_inner()) * self.u_diff1() * self.u_diff1() * sol_new.k_inner() / self.k_lim_inner() \
            - self.c_mu * (self.eps_hat_inner() / self.eps_lim_inner()) * self.k_inner() * sol_new.k_inner()
    def eps_equation(self, sol_new):
        return self.eps_diffusion(sol_new) \
            - self.v_inner() * sol_new.eps_diff1() \
            + self.eps_convection(sol_new) \
            + self.c_eps_1 * (self.eps_hat_inner() / self.eps_lim_inner()) * self.u_diff1() * self.u_diff1() * sol_new.eps_inner() / self.k_lim_inner() \
            - self.c_eps_2 * (self.eps_hat_inner() / self.eps_lim_inner()) * self.c_mu * self.k_inner() * sol_new.eps_inner()
    def eps_hat_equation(self, sol_new):
        return self.eps_hat_diffusion(sol_new) \
            - self.v_inner() * sol_new.eps_hat_diff1() \
            + self.eps_hat_convection(sol_new) \
            + self.c_eps_hat_1 * (self.eps_hat_inner() * sol_new.eps_hat_inner() / self.eps_lim_inner()) * self.u_diff1() * self.u_diff1() / self.k_lim_inner() \
            + self.c_eps_hat_1_hat * self.eps_lim_inner() * self.u_diff1() * self.u_diff1() / self.k_lim_inner() \
            - self.c_eps_hat_2 * (self.eps_hat_inner() * sol_new.eps_hat_inner() / self.eps_lim_inner()) * self.c_mu * self.k_inner()
    def plot(self, label):
        super().plot(label)
        sol_new = self
        xs = self.inner(self.grid())
        self.ax_u_terms.cla()
        self.ax_u_terms.plot(xs, - self.v_inner() * self.u_diff1() + self.u_convection(self), label="Conv")
        self.ax_u_terms.plot(xs, self.u_diffusion(self), label="Diff")
        self.ax_u_terms.legend()
        self.fig_u_terms.savefig(self.data_prefix + self.plot_prefix + "-" + "u-equation-terms" + "-plot.pdf")
        self.ax_k_terms.cla()
        self.ax_k_terms.plot(xs, - self.v_inner() * self.k_diff1() + self.k_convection(self), label="Conv")
        self.ax_k_terms.plot(xs, + (self.eps_hat_inner() / self.eps_lim_inner()) * self.u_diff1() * self.u_diff1() * self.k_inner() / self.k_lim_inner(), label="Prod")
        self.ax_k_terms.plot(xs, - self.c_mu * (self.eps_hat_inner() / self.eps_lim_inner()) * self.k_inner() * self.k_inner() , label="Diss")
        self.ax_k_terms.plot(xs, self.k_diffusion(self), label="Diff")
        self.ax_k_terms.legend()
        self.fig_k_terms.savefig(self.data_prefix + self.plot_prefix + "-" + "k-equation-terms" + "-plot.pdf")
        self.ax_eps_terms.cla()
        self.ax_eps_terms.plot(xs, - self.v_inner() * sol_new.eps_diff1() + self.eps_convection(self), label="Conv")
        self.ax_eps_terms.plot(xs, + self.c_eps_1 * (self.eps_hat_inner() / self.eps_lim_inner()) * self.u_diff1() * self.u_diff1() * sol_new.eps_inner() / self.k_lim_inner() , label="Prod")
        self.ax_eps_terms.plot(xs, - self.c_eps_2 * (self.eps_hat_inner() / self.eps_lim_inner()) * self.c_mu * self.k_inner() * sol_new.eps_inner(), label="Diss")
        self.ax_eps_terms.plot(xs, self.eps_diffusion(self), label="Diff")
        self.ax_eps_terms.legend()
        self.fig_eps_terms.savefig(self.data_prefix + self.plot_prefix + "-" + "eps-equation-terms" + "-plot.pdf")
        self.ax_epshat_terms.cla()
        self.ax_epshat_terms.plot(xs, - self.v_inner() * sol_new.eps_hat_diff1() + self.eps_hat_convection(self), label="Conv")
        self.ax_epshat_terms.plot(xs, + self.c_eps_hat_1 * (self.eps_hat_inner() * sol_new.eps_hat_inner() / self.eps_lim_inner()) * self.u_diff1() * self.u_diff1() / self.k_lim_inner() , label="Prod")
        self.ax_epshat_terms.plot(xs, - self.c_eps_hat_2 * (self.eps_hat_inner() * sol_new.eps_hat_inner() / self.eps_lim_inner()) * self.c_mu * self.k_inner(), label="Diss")
        self.ax_epshat_terms.plot(xs, self.eps_hat_diffusion(self), label="Diff")
        self.ax_epshat_terms.legend()
        self.fig_epshat_terms.savefig(self.data_prefix + self.plot_prefix + "-" + "epshat-equation-terms" + "-plot.pdf")
        self.ax_n.plot(self.grid(), self.n(), label=label)
        self.ax_n.legend()
        self.fig_n.savefig(self.data_prefix + self.plot_prefix + "-" + "turbulent-viscosity" + "-plot.pdf")


def perform_intermediate_keps_jet_simulation(timestepper=crank_nicholson_step):
    L = 50
    J = 101
    dx = float(L)/float(J-1)
    x_grid = np.array([j*dx for j in range(J)])
    dt = 1e1

    # initialize calculation with reasonable values:
    u = np.array([1 / np.cosh(x/17)**2 for x in x_grid])
    k = np.array([0.06 / np.cosh(x/17)**2 for x in x_grid])
    eps = np.array([0.1 / np.cosh(x/17)**2 for x in x_grid])
    eps_hat = np.array([0.1 / np.cosh(x/17)**2 for x in x_grid])
    sol = Intermediate_KEps_Jet_Solution(x_grid, u, k, eps, eps_hat)

    # alternatively, to load an existing simlulation:
    # prefix = Intermediate_KEps_Jet_Solution.plot_prefix
    # sol = Intermediate_KEps_Jet_Solution.from_file(Intermediate_KEps_Jet_Solution.data_prefix + prefix + "-out" + ".h5", prefix=prefix, iteration=561)

    sol.x_grid = x_grid

    sol.dt = dt
    sol.ensure_mom_cons()

    perform_simulation(sol, timestepper, plot_interval = 20, max_iter = 1000, tolerance = 1e-30)
