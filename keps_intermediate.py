#!/usr/bin/env python3

from keps_classical import *

class Intermediate_KEps_Solution(Classical_KEps_Solution):
    eps_hat_min = 1e-10
    # new constants
    c_eps_hat_2 = 1.92
    c_eps_hat_1 = 1.42
    c_eps_2 = 1.92
    c_eps_1 = 1.44
    sigma_k = 0.91
    c_eps_hat_1_super = 1.42
    c_eps_hat_1 = c_eps_hat_1_super * 1.0
    c_eps_hat_1_hat = c_eps_hat_1_super * 0.05
    sigma_eps_hat = 1.253
    sigma_eps = 1.3
    sigma_eps_hat = sigma_eps * (c_eps_1 - c_eps_2) / (c_eps_hat_1 - c_eps_hat_2)

    def __init__(self, x_grid, u, k, eps, eps_hat):
        Solution.__init__(self, x_grid, np.array([u, k, eps, eps_hat]))
    @classmethod
    def from_file(cls, filename, iteration = None, prefix = None):
        x, fields, restart_iter, restart_time, change_vec  = cls.read_from_file(filename, iteration, prefix)
        new_inst = cls(x, fields[0], fields[1], fields[2], fields[3])
        new_inst.iteration = restart_iter
        new_inst.t = restart_time
        new_inst.change_vec = change_vec
        return new_inst
    @classmethod
    def from_classical_model_solution(cls, classical_sol, prefix = None):
        return cls(classical_sol.grid(), classical_sol.u(), classical_sol.k(), classical_sol.eps(), classical_sol.eps())
    def init_plotting(self):
        super().init_plotting()
        self.multi_fig, self.multi_axs = plt.subplots(2,2,figsize = (50,30), dpi=500)
        self.fig_n, self.ax_n = plt.subplots()
        self.fig_u_terms, self.ax_u_terms = plt.subplots()
        self.fig_k_terms, self.ax_k_terms = plt.subplots()
        self.fig_eps_terms, self.ax_eps_terms = plt.subplots()
        self.fig_epshat_terms, self.ax_epshat_terms = plt.subplots()
    def set_sol(self, new_sol):
        u_k_eps_epshat = new_sol.reshape(self.number_of_fields(), self.number_of_nodes())
        self.update_change(new_sol)
        self.fields = u_k_eps_epshat
        self.advance_timestep()
        self.update_cached_fields()
    def make_zero_solution(self):
        return Intermediate_KEps_Solution(self.grid(), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()))
    def make_unit_solution(self, i):
        u_k_eps_epshat = np.eye(self.size())[i].reshape(self.number_of_fields(), self.number_of_nodes())
        return Intermediate_KEps_Solution(self.grid(), u_k_eps_epshat[0], u_k_eps_epshat[1], u_k_eps_epshat[2], u_k_eps_epshat[3])
    # def eps_tilde(self):
    #     return self.fields[2]
    def eps(self):
        # return np.exp(self.eps_tilde())
        return self.fields[2]
    def eps_tilde_diff1(self):
        return self.diff1(self.eps_tilde())
    def eps_tilde_diff2(self):
        return self.diff2(self.eps_tilde())
    def eps_lim(self):
        return self.eps()
    # def eps_tilde_inner(self):
    #     return self.inner(self.eps_tilde())
    # def eps_tildel(self):
    #     return self.left(self.eps_tilde())
    # def eps_tilder(self):
    #     return self.right(self.eps_tilde())
    def eps_hat(self):
        return self.fields[3]
    def eps_hat_inner(self):
        return self.inner(self.eps_hat())
    def eps_hatl(self):
        return self.left(self.eps_hat())
    def eps_hatr(self):
        return self.right(self.eps_hat())
    def eps_hat_diff1(self):
        return self.diff1(self.eps_hat())
    def eps_hat_diff2(self):
        return self.diff2(self.eps_hat())
    def eps_hat_lim(self):
        return np.array([max(eps_hat, self.eps_hat_min) for eps_hat in self.eps_hat()])
    def eps_hat_lim_inner(self):
        return self.inner(self.eps_hat_lim())
    def eps_hatl_lim(self):
        return self.left(self.eps_hat_lim())
    def eps_hatr_lim(self):
        return self.right(self.eps_hat_lim())
    def full_equation(self, sol_intermediate, sol_new):
        return np.concatenate(([(sol_new.ul() - self.u_bc_left())], (sol_new.u_inner() - self.u_inner()) - self.dt * self.u_equation(sol_intermediate), [(sol_new.ur() - self.u_bc_right())], \
                          [(sol_new.kl() - self.k_bc_left())], (sol_new.k_inner() - self.k_inner()) - self.dt * self.k_equation(sol_intermediate), [(sol_new.kr() - self.k_bc_right())], \
                          [(sol_new.epsl() - self.eps_bc_left())], (sol_new.eps_inner() - self.eps_inner()) - self.dt * self.eps_equation(sol_intermediate), [(sol_new.epsr() - self.eps_bc_right())], \
                          # [(sol_new.eps_tildel() - self.eps_tilde_bc_left())], (sol_new.eps_tilde_inner() - self.eps_tilde_inner()) - self.dt * self.eps_tilde_equation(sol_intermediate), [(sol_new.eps_tilder() - self.eps_tilde_bc_right())], \
                          [(sol_new.eps_hatl() - self.eps_hat_bc_left())], (sol_new.eps_hat_inner() - self.eps_hat_inner()) - self.dt * self.eps_hat_equation(sol_intermediate), [(sol_new.eps_hatr() - self.eps_hat_bc_right())]))
    def field_name_list(self):
        return ["u", "k", "\\varepsilon", "\\varepsilon_{hat}"]
    def plot(self, label):
        super().plot(label)
        multi_fig = self.multi_fig
        multi_axs = self.multi_axs
        field_names = self.field_name_list()
        field_index = 0
        for i in range(2):
            for j in range(2):
                multi_axs[i][j].plot(self.grid(), self.fields[field_index], label = label)
                multi_axs[i][j].set_xlabel("$x$")
                multi_axs[i][j].set_ylabel("$" + field_names[field_index] + "$")
                field_index += 1
        multi_axs[0][0].legend(fontsize="x-small")
        multi_fig.savefig(self.data_prefix + self.plot_prefix + "-" + "combined" + "-plot.pdf")
