#!/usr/bin/env python3

import sys
import getopt
import io

import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify

from navier_stokes_jet import perform_ns_jet_simulation
from keps_classical_jet import perform_classical_keps_jet_simulation
from keps_intermediate_jet import perform_intermediate_keps_jet_simulation
from keps_new_model_jet import perform_new_keps_jet_simulation

def main():
    model = sys.argv[1]
    print("calculating jet")
    if model == "ns" or model == "0":
        print("using Navier-Stokes")
        perform_ns_jet_simulation()
    elif model == "classical" or model == "1":
        print("using classical k-epsilon model")
        perform_classical_keps_jet_simulation()
    elif model == "intermediate" or model == "2":
        print("using intermediate k-epsilon model")
        perform_intermediate_keps_jet_simulation()
    elif model == "new" or model == "3":
        print("using new k-epsilon model")
        perform_new_keps_jet_simulation()
    else:
        raise Exception("Unable to interpret input " + model +" \n \
        valid inputs include:\n \
        \"ns\" or 0 for Navier-Stokes solver (constant viscosity) \n \
        \"classical\" or 1 for classical k-epsilon model \n \
        \"intermediate\" or 2 for intermediate k-epsilon model")
    Notify.init("Python FDS")
    Notify.Notification.new("Done!", "Calculation of " + "plane jet" + " using " + model + " model finished", "/usr/lib/python3.10/idlelib/Icons/idle_16.png").show()

main()
