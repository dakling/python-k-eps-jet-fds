#!/usr/bin/env python3

from solution import *

class NS_Solution(Solution):
    n_const = 1e-2
    plot_prefix = "navier-stokes"
    def __init__(self, x_grid, u):
        Solution.__init__(self, x_grid, np.array([u]))
    @classmethod
    def from_file(cls, filename, iteration = None, prefix = None):
        x, fields, restart_iter, restart_time, change_vec = cls.read_from_file(filename, iteration, prefix)
        new_inst = cls(x, fields[0])
        new_inst.iteration = restart_iter
        new_inst.t = restart_time
        new_inst.change_vec = change_vec
        return new_inst
    def init_plotting(self):
        super().init_plotting()
        self.fig_transformed, self.ax_transformed = plt.subplots()
    def make_zero_solution(self):
        return NS_Solution(self.grid(), np.zeros(self.size()))
    def make_unit_solution(self, i):
        return NS_Solution(self.grid(), np.eye(self.size())[i])
    def u(self):
        return self.fields[0]
    def set_sol(self, new_sol):
        self.advance_timestep()
        self.update_change(new_sol)
        self.fields[0] = new_sol
        self.update_cached_fields()
    def u_inner(self):
        return self.inner(self.u())
    def ul(self):
        return self.left(self.u())
    def ur(self):
        return self.right(self.u())
    def u_diff1(self):
        return self.diff1(self.u())
    def u_diff2(self):
        return self.diff2(self.u())
    def n(self):
        return self.n_const * np.ones(self.number_of_nodes())
    def n_inner(self):
        return self.inner(self.n())
    def calculate_eta(self):
        J = self.number_of_nodes()
        eta = np.zeros(J)
        for i in range(J):
            if i == J:
                eta[i] = self.eta_e
            else:
                integrand = self.n()
                eta[i] = self.eta_e + simpson(integrand[0:i+1], x = self.grid()[0:i+1])
        return eta
    def update_eta(self):
        self.cached_fields[1] = self.calculate_eta()
    def eta(self):
        return self.cached_fields[1]
    def eta_inner(self):
        return self.inner(self.eta())
    def full_equation(self, sol_intermediate, sol_new):
        return np.concatenate(([(sol_new.ul() - self.u_bc_left())], (sol_new.u_inner() - self.u_inner()) - self.dt * self.u_equation(sol_intermediate), [(sol_new.ur() - self.u_bc_right())]))
    def field_name_list(self):
        return ["u"]
    def plot_transformed(self):
        field_names = self.field_name_list()
        self.ax_transformed.cla()
        self.plot_reference_solution()
        self.ax_transformed.plot(self.eta(), self.u(), label = "Numerical Solution")
        self.ax_transformed.set_xlabel("$\\eta$")
        self.ax_transformed.set_ylabel("$" + field_names[0] + "$")
        self.ax_transformed.legend()
        self.fig_transformed.savefig(self.data_prefix + self.plot_prefix + "-plot-transformed.pdf")

