# python-k-eps-jet-fds

This code calculates the plane turbulent jet using a simple constant-eddy-viscosity model, the classical k-epsilon model or a modified k-epsilon model taking into account additional statistical symmetry constraints. More details can be found in the corresponding pubication at tbd.

## Usage

For quick calculations, the convenient script `main.py` is provided. It can be invoked with 

```
python main.py $model
```
where `$model` can be "ns" for a simple Navier-Stokes type model with a constant eddy viscosity (this model is only available for testing purposes, do not expect reasonable results from it), "classical" for the standard k-epsilon model, or "intermediate" or "new" for modified models presented in the publication. Note that the latter two models differ in the convective terms, where the intermediate model uses the mean velocity, and the new model uses a different velocity field.
The results and plots are written to the folder "./data-output".

For more complicated studies, it makes sense to have all input and output files in a single place, as exemplified by the folders in "./calulations". This is the simulation that was used to obtain Figure 1 in the publication.
