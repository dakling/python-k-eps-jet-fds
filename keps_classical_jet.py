#!/usr/bin/env python3

from navier_stokes_jet import *
from keps_classical import *

class Classical_KEps_Jet_Solution(Classical_KEps_Solution, NS_Jet_Solution):

    plot_prefix = "classical-keps-jet"

    def set_sol(self, new_sol):
        u_k_eps = new_sol.reshape(self.number_of_fields(), self.number_of_nodes())
        u_relaxed = self.a_u * u_k_eps[0] + (1 - self.a_u) * self.u()
        k_relaxed = self.a_k * u_k_eps[1] + (1 - self.a_k) * self.k()
        eps_relaxed = self.a_eps * u_k_eps[2] + (1 - self.a_eps) * self.eps()
        u_k_eps_relaxed = np.stack((u_relaxed, k_relaxed, eps_relaxed))
        self.advance_timestep()
        u_new, k_new, eps_new = self.ensure_mom_cons(u_k_eps_relaxed)
        new_sol = np.stack((u_new, k_new, eps_new))
        self.update_change(new_sol)
        self.fields = new_sol
        self.update_cached_fields()
    def ensure_mom_cons(self, new_sol):
        u = new_sol[0]
        k = new_sol[1]
        eps = new_sol[2]
        sqrt_J_over_x = 1 / u[0]
        u_norm = u * sqrt_J_over_x
        k_norm = k * sqrt_J_over_x**2
        eps_norm = eps * sqrt_J_over_x**3
        return (u_norm, k_norm, eps_norm)
    def update_cached_fields(self):
        self.update_n()
        self.update_eta()
        self.update_v()
    def v(self):
        return self.cached_fields[2]
    def update_v(self):
        self.cached_fields[2] = self.calculate_v()
    # boundary conditions
    def k_bc_left(self):
        return self.k()[1]
    def k_bc_right(self):
        return self.k_min
    def eps_bc_left(self):
        return self.eps()[1]
    def eps_bc_right(self):
        return self.eps_min
    # equations
    def k_convection(self, sol_new):
        return self.u_inner() * self.n_inner() * sol_new.k_inner()
    def k_diffusion(self, sol_new):
        return 1/self.sigma_k * sol_new.k_diff2()
    def eps_convection(self, sol_new):
        return 5/2 * self.u_inner() * self.n_inner() * sol_new.eps_inner()
    def eps_diffusion(self, sol_new):
        return 1/self.sigma_eps * sol_new.eps_diff2()
    def k_equation(self, sol_new):
        return self.k_diffusion(sol_new) \
            - self.v_inner() * sol_new.k_diff1() \
            + self.k_convection(sol_new) \
            + self.u_diff1() * self.u_diff1() * sol_new.k_inner() / self.k_lim_inner() \
            - self.c_mu * self.k_inner() * sol_new.k_inner()
    def eps_equation(self, sol_new):
        return self.eps_diffusion(sol_new) \
            - self.v_inner() * sol_new.eps_diff1() \
            + self.eps_convection(sol_new) \
            + self.c_eps_1 * self.u_diff1() * self.u_diff1() * sol_new.eps_inner() / self.k_lim_inner() \
            - self.c_eps_2 * self.c_mu * self.k_inner() * sol_new.eps_inner()
    def init_cached_fields(self):
        n = self.calculate_n()
        self.cached_fields = np.array([n])
        eta = self.calculate_eta()
        self.cached_fields = np.stack((n, eta))
        v = self.calculate_v()
        self.cached_fields = np.stack((n, eta,v))
    def init_plotting(self):
        self.fig_transformed, self.ax_transformed = plt.subplots()
        for i in range(self.no_of_fields + 1):
            fig, ax = plt.subplots()
            self.figs.append(fig)
            self.axs.append(ax)
    def plot(self, label):
        field_names = self.field_name_list()
        number_of_fields = self.number_of_fields()
        for i in range(number_of_fields):
            self.axs[i].plot(self.grid(), self.fields[i], label = label)
            self.axs[i].set_xlabel("$x$")
            self.axs[i].set_ylabel("$" + field_names[i] + "$")
            self.axs[i].legend(fontsize="x-small")
            self.figs[i].savefig(self.data_prefix + self.plot_prefix + "-" + field_names[i] + "-plot.pdf")
        self.axs[number_of_fields].plot(self.grid(), self.v(), label = label)
        self.axs[number_of_fields].legend(fontsize="x-small")
        self.figs[number_of_fields].savefig(self.data_prefix + self.plot_prefix + "-" + "v" + "-plot.pdf")
    def plot_reference_solution(self):
        root_dir = str(Path(__file__).parent.absolute())
        exp_data = Jet_Solution.from_file(root_dir + "/data-input/jet-experimental.csv")
        self.ax_transformed.plot(exp_data.grid(), exp_data.fields[0], ".", label = "experimental data")


def perform_classical_keps_jet_simulation(timestepper=crank_nicholson_step):
    L = 150
    J = 101
    dx = float(L)/float(J-1)
    x_grid = np.array([j*dx for j in range(J)])
    dt = 1e1


    # initialize calculation with reasonable values:
    u = np.array([1 / np.cosh(x/17)**2 for x in x_grid])
    k = np.array([0.06 / np.cosh(x/17)**2 for x in x_grid])
    eps = np.array([0.1 / np.cosh(x/17)**2 for x in x_grid])
    sol = Classical_KEps_Jet_Solution(x_grid, u, k, eps)

    # alternatively, to load an existing simlulation:
    # prefix = Classical_KEps_Jet_Solution.plot_prefix + "-u-new-norm"
    # sol = Classical_KEps_Jet_Solution.from_file(Classical_KEps_Jet_Solution.data_prefix + prefix + "-out" + ".h5", prefix=prefix)

    sol.dt = dt

    perform_simulation(sol, timestepper, plot_interval = 50, max_iter = 1000, tolerance=1e-30)
