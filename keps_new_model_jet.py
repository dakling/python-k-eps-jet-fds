#!/usr/bin/env python3

from keps_new_model import *
from keps_intermediate_jet import *

class New_KEps_Jet_Solution(New_KEps_Solution, Intermediate_KEps_Jet_Solution):

    plot_prefix = "new-keps-jet"

    a_u = 0.7
    a_k = 0.7
    a_eps = 0.7
    a_eps_hat = 0.7
    a_u_hat = 0.7

    def set_sol(self, new_sol):
        u_k_eps = new_sol.reshape(self.number_of_fields(), self.number_of_nodes())
        u_relaxed = self.a_u * u_k_eps[0] + (1 - self.a_u) * self.u()
        k_relaxed = self.a_k * u_k_eps[1] + (1 - self.a_k) * self.k()
        eps_relaxed = self.a_eps * u_k_eps[2] + (1 - self.a_eps) * self.eps()
        eps_hat_relaxed = self.a_eps_hat * u_k_eps[3] + (1 - self.a_eps_hat) * self.eps_hat()
        u_hat_relaxed = self.a_u_hat * u_k_eps[4] + (1 - self.a_u_hat) * self.u_hat()
        u_k_eps_relaxed = np.stack((u_relaxed, k_relaxed, eps_relaxed, eps_hat_relaxed, u_hat_relaxed))
        u_k_eps_cons = self.ensure_mom_cons(u_k_eps_relaxed)
        success = self.update_change(u_k_eps_cons)
        if success:
            self.advance_timestep()
            self.fields = u_k_eps_cons
        self.update_cached_fields()
    def ensure_mom_cons(self, new_sol=None):
        if new_sol is None:
            new_sol_safe = self.fields
        else:
            new_sol_safe = new_sol
        u = new_sol_safe[0]
        k = new_sol_safe[1]
        eps = new_sol_safe[2]
        eps_hat = new_sol_safe[3]
        u_hat = new_sol_safe[4]
        sqrt_J_over_x = 1 / (u[0])
        sqrt_J_hat_over_x = 1 / (u_hat[0])
        a_s = sqrt_J_over_x / sqrt_J_hat_over_x
        u_norm = u * sqrt_J_hat_over_x * a_s
        k_norm = k * sqrt_J_hat_over_x**2 * a_s
        eps_norm = eps * sqrt_J_hat_over_x**3 * a_s**2
        eps_hat_norm = eps_hat * sqrt_J_hat_over_x**3 * a_s
        u_hat_norm = u_hat * sqrt_J_hat_over_x
        if new_sol is None:
            self.fields[0] = u_norm
            self.fields[1] = k_norm
            self.fields[2] = eps_norm
            self.fields[3] = eps_hat_norm
            self.fields[4] = u_hat_norm
        return np.stack((u_norm, k_norm, eps_norm, eps_hat_norm, u_hat_norm))
    def calculate_v(self):
        J = self.number_of_nodes()
        v = np.zeros(J)
        for i in range(J):
            if i == 0:
                v[i] = 0
            else:
                integrand = - 1/2 * self.u_hat() * self.n()
                v[i] = simpson(integrand[0:i], x = self.grid()[0:i])
        return v
    # boundary conditions
    def u_hat_bc_left(self):
        return self.u_hat()[1]
    def u_hat_bc_right(self):
        return 0
    # equations
    def k_convection(self, sol_new):
            return self.u_hat_inner() * self.n_inner() * sol_new.k_inner()
    def eps_convection(self, sol_new):
            return 5/2 * self.u_hat_inner() * self.n_inner() * sol_new.eps_inner()
    def eps_hat_convection(self, sol_new):
            return 5/2 * self.u_hat_inner() * self.n_inner() * sol_new.eps_hat_inner()
    def eps_hat_equation(self, sol_new):
        return self.eps_hat_diffusion(sol_new) \
            - self.v_inner() * sol_new.eps_hat_diff1() \
            + self.eps_hat_convection(sol_new) \
            + self.c_eps_hat_1 * (self.eps_hat_inner() * sol_new.eps_hat_inner() / self.eps_lim_inner()) * self.u_diff1() * self.u_diff1() / self.k_lim_inner() \
            + self.c_eps_hat_1_hat * self.eps_lim_inner() * self.u_diff1() * self.u_diff1() / self.k_lim_inner() \
            - self.c_eps_hat_2 * (self.eps_hat_inner() * sol_new.eps_hat_inner() / self.eps_lim_inner()) * self.c_mu * self.k_inner()
    def u_hat_convection(self, sol_new):
        return + 1/2 * self.u_hat_inner() * self.n_inner() * sol_new.u_hat_inner()
    def u_hat_diffusion(self, sol_new):
        return self.iota * sol_new.u_hat_diff2()
    def u_hat_equation(self, sol_new):
        return self.u_hat_diffusion(sol_new) \
            - self.v_inner() * sol_new.u_hat_diff1() \
            + self.u_hat_convection(sol_new)
    def plot(self, label):
        super().plot(label)
        sol_new = self
        xs = self.inner(self.grid())
        self.ax_u_hat_terms.cla()
        self.ax_u_hat_terms.plot(xs, - self.v_inner() * self.u_hat_diff1() + self.u_hat_convection(self), label="Conv")
        self.ax_u_hat_terms.plot(xs, self.u_diffusion(self), label="Diff")
        self.ax_u_hat_terms.legend()
        self.fig_u_hat_terms.savefig(self.data_prefix + self.plot_prefix + "-" + "u-hat-equation-terms" + "-plot.pdf")


def perform_new_keps_jet_simulation(timestepper=crank_nicholson_step):
    L = 170
    J = 151
    dx = float(L)/float(J-1)
    x_grid = np.array([j*dx for j in range(J)])
    dt = 1e1

    # initialize calculation with reasonable values:
    u = np.array([1 / np.cosh(x/17)**2 for x in x_grid])
    k = np.array([0.06 / np.cosh(x/17)**2 for x in x_grid])
    eps = np.array([0.1 / np.cosh(x/17)**2 for x in x_grid])
    eps_hat = np.array([0.1 / np.cosh(x/17)**2 for x in x_grid])
    u_hat = np.array([1 / np.cosh(x/17)**2 for x in x_grid])
    sol = New_KEps_Jet_Solution(x_grid, u, k, eps, eps_hat, u_hat)

    # alternatively, to load an existing simlulation:
    # prefix = New_KEps_Jet_Solution.plot_prefix
    # classical_sol = Classical_KEps_Jet_Solution.from_file(Classical_KEps_Jet_Solution.data_prefix + Classical_KEps_Jet_Solution.plot_prefix + "-out" + ".h5")
    # sol = New_KEps_Jet_Solution.from_classical_model_solution(classical_sol)

    sol.dt = dt
    sol.ensure_mom_cons()
    perform_simulation(sol, timestepper, plot_interval = 20, max_iter = 1000, tolerance = 1e-30)
