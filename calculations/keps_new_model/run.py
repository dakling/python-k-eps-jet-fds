#!/usr/bin/env python3

import sys
sys.path.append( '../../' )
from keps_new_model_jet import *

class New_KEps_Jet_Smaller_Epshat_Solution(New_KEps_Jet_Solution):
    data_prefix = "./"
    plot_prefix = "new-keps-jet"


def perform_new_keps_jet_smaller_epshat_simulation(timestepper=crank_nicholson_step):
    L = 170
    J = 101
    dx = float(L)/float(J-1)
    x_grid = np.array([j*dx for j in range(J)])
    dt = 1e0

    prefix = New_KEps_Jet_Smaller_Epshat_Solution.plot_prefix
    sol = New_KEps_Jet_Smaller_Epshat_Solution.from_file("./new-keps-jet-out.h5", prefix=prefix)
    sol.dt = dt
    sol.ensure_mom_cons()

    perform_simulation(sol, timestepper, plot_interval = 20, max_iter = 555002, tolerance = 1e-40)

print("Starting simulation")
perform_new_keps_jet_smaller_epshat_simulation()
