#!/usr/bin/env python3

from navier_stokes import *
from jet import *

class NS_Jet_Solution(NS_Solution, Jet_Solution):
    a_u = 0.7
    def set_sol(self, new_sol):
        self.advance_timestep()
        old_sol = self.u()
        new_sol_relaxed = new_sol * self.a_u + (1 - self.a_u) * old_sol
        self.update_change(new_sol_relaxed)
        self.fields[0] = new_sol_relaxed
        self.ensure_mom_cons()
        self.update_cached_fields()
    def init_cached_fields(self):
        self.cached_fields = np.array([self.calculate_eta(), self.calculate_v()])
    def update_cached_fields(self):
        self.cached_fields[0] = self.calculate_eta()
        self.cached_fields[1] = self.calculate_v()
    def calculate_v(self):
        J = self.number_of_nodes()
        v = np.zeros(J)
        for i in range(J):
            if i == 0:
                v[i] = 0
            else:
                integrand = - 1/2 * self.u() * self.n()
                v[i] = simpson(integrand[0:i], x = self.grid()[0:i])
        return v
    def update_v(self):
        self.cached_fields[1] = self.calculate_v()
    def v(self):
        return self.cached_fields[1]
    def v_inner(self):
        return self.inner(self.v())
    def u_bc_left(self):
        return self.u()[1]
    def u_bc_right(self):
        return 0
    def u_convection(self, sol_new):
        return + 1/2 * self.u_inner() * self.n_inner() * sol_new.u_inner()
    def u_diffusion(self, sol_new):
        return sol_new.u_diff2()
    def u_equation(self, sol_new):
        return self.u_diffusion(sol_new) \
            - self.v_inner() * sol_new.u_diff1() \
            + self.u_convection(sol_new)

def perform_ns_jet_simulation(timestepper=crank_nicholson_step):
    L = 100
    J = 101
    dx = float(L)/float(J-1)
    x_grid = np.array([j*dx for j in range(J)])
    dt = 1e1

    # initialize calculation with reasonable values:
    u = np.array([1 / np.cosh(x/17)**2 for x in x_grid])
    sol = NS_Jet_Solution(x_grid, u)
    sol.dt = dt
    sol.ensure_mom_cons()

    perform_simulation(sol, timestepper, plot_interval = 1, max_iter = 5)
