#!/usr/bin/env python3

import numpy as np
import scipy as sp
from matplotlib import pyplot
from scipy.integrate import simpson
import matplotlib.pyplot as plt
import numbers
import copy
import csv
import h5py

class Solution:
    t = 0
    iteration = 0
    max_iteration = 300
    dt = 1e-1
    tolerance = 1e-20
    change_upper_limit = 100
    change_lower_limit = 1e-5
    a_u = 0.7
    def __init__(self, x_grid, fields):
        self.change = 1000
        self.x_grid = x_grid
        self.fields = fields
        self.old_fields = fields
        self.no_of_nodes = len(x_grid)
        self.no_of_fields = len(fields)
        self.cached_fields = None
        self.init_cached_fields()
        self.figs = []
        self.axs = []
    @classmethod
    def from_file(cls, filename, iteration = None, prefix = None):
        x, fields, restart_iter, restart_time, change_vec = cls.read_from_file(filename, iteration, prefix)
        new_inst = cls(x, fields)
        new_inst.iteration = restart_iter
        new_inst.t = restart_time
        new_inst.change_vec = change_vec
        return cls(x, fields)
    @classmethod
    def read_from_file(cls, filename, iteration = None, prefix = None):
        filetype = filename.split(".")[-1]
        if filetype == "hd5" or filetype == "h5":
            return cls.read_from_hd5_file(filename, iteration, prefix)
        elif filetype == "csv" or filetype == "dat":
            return cls.read_from_csv_file(filename, iteration)
        else:
            raise Exception("Unknown file type")
    @classmethod
    def read_from_hd5_file(cls, filename, restart_iteration = None, prefix = None):
        if prefix is None:
            pp = cls.plot_prefix
        else:
            pp = prefix
        with h5py.File(filename) as f:
            calculation_grp = f[pp]
            if restart_iteration is None:
                restart_iter = calculation_grp.attrs.get("last_iteration")
            else:
                restart_iter = restart_iteration
            timestep_grp = calculation_grp["iter" + str(restart_iter)]
            # try:
            restart_time = timestep_grp.attrs.get("last_time")
            change_vec = timestep_grp.attrs.get("change_vec")
            x = timestep_grp["x"][:]
            fields = timestep_grp["fields"][:]
            return (x, fields, restart_iter, restart_time, change_vec)
    @classmethod
    def read_from_csv_file(cls, filename, iteration):
        with open(filename) as csvDataFile:
            csvReader = csv.reader(csvDataFile)
            ret_list = []
            for row in csvReader:
                row_formatted = [float(elem) for elem in row]
                ret_list.append(row_formatted)
            ret = np.transpose(np.array(ret_list))
            return (ret[0], ret[1:], iteration, 0, [])
    def write_to_file(self, filename, addition = ""):
        with h5py.File(filename, 'a') as f:
            if addition == "":
                addn = "iter" + str(self.iteration)
            else:
                addn = addition
            calculation_grp = f.require_group(self.plot_prefix)
            timestep_grp = calculation_grp.require_group(addn)
            try: # if fields already exist, this only works if shapes match, otherwise, a type error is raised
                x_data = timestep_grp.require_dataset("x", shape = self.grid().shape, dtype = "f")
                field_data = timestep_grp.require_dataset("fields", shape = self.fields.shape, dtype = "f")
                x_data[...] = self.grid()
                field_data[...] = self.fields
            except TypeError:
                del(timestep_grp["x"])
                del(timestep_grp["fields"])
                timestep_grp.create_dataset("x", data = self.grid())
                timestep_grp.create_dataset("fields", data = self.fields)
            calculation_grp.attrs.modify("last_iteration", self.iteration)
            timestep_grp.attrs.modify("last_time", self.t)
            timestep_grp.attrs.modify("change_vec", self.change_vec)
    def write_to_csv_file(self, filename):
        with open(filename, 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            # writer.writerow(header)
            data = np.insert(np.transpose(self.fields), 0, np.transpose(self.grid()), axis=1)
            writer.writerows(data)
    def update_change(self, new_sol):
        difference = new_sol - self.fields
        change_new = [0 for i in range(self.number_of_fields())]
        for i in range(self.number_of_fields()):
            change_new[i] = np.dot(difference[i], difference[i])/np.dot(self.fields[i],self.fields[i]) / self.number_of_nodes()
        self.change_vec = change_new
        self.change = np.dot(change_new, change_new)
        return True
    def number_of_fields(self):
        return self.no_of_fields
    def number_of_nodes(self):
        return self.no_of_nodes
    def field_name_list(self):
        return ["var" + str(i) for i in range(self.number_of_fields())]
    def size(self):
        return self.number_of_fields() * self.number_of_nodes()
    def grid(self):
        return self.x_grid
    def fields_as_one(self):
        return self.fields.reshape(self.number_of_fields() * self.number_of_nodes())
    def inner(self, field):
        return field[1:-1]
    def left(self, field):
        return field[0]
    def right(self, field):
        return field[-1]
    def diff1(self, field):
        return diff1(self.inner(field), self.left(field), self.right(field), self.grid())
    def diff2(self, field):
        return diff2(self.inner(field), self.left(field), self.right(field), self.grid())
    def __add__(self, other_sol):
        ret = copy.deepcopy(self)
        n = self.number_of_fields()
        if n != other_sol.number_of_fields():
            raise Exception("Attempt to add incompatible Solutions")
        ret.fields = [self.fields[i] + other_sol.fields[i] for i in range(n)]
        return ret
    def __sub__(self, other_sol):
        ret = copy.deepcopy(self)
        n = self.number_of_fields()
        if n != other_sol.number_of_fields():
            raise Exception("Attempt to subtract incompatible Solutions")
        ret.fields = [self.fields[i] - other_sol.fields[i] for i in range(n)]
        return ret
    def __mul__(self, other):
        ret = copy.deepcopy(self)
        n = self.number_of_fields()
        if not isinstance(other, numbers.Number):
            if n != other.number_of_fields():
                raise Exception("Attempt to multiply incompatible Solutions")
            ret.fields = [self.fields[i] * other.fields[i] for i in range(n)]
        else:
            ret.fields = [self.fields[i] * other for i in range(n)]
        return ret
    def __truediv__(self, other):
        ret = copy.deepcopy(self)
        n = self.number_of_fields()
        if not isinstance(other, numbers.Number):
            if n != other.number_of_fields():
                raise Exception("Attempt to multiply incompatible Solutions")
            ret.fields = [self.fields[i] / other.fields[i] for i in range(n)]
        else:
            ret.fields = [self.fields[i] / other for i in range(n)]
        return ret
    def init_plotting(self):
        for i in range(self.no_of_fields):
            fig, ax = plt.subplots()
            self.figs.append(fig)
            self.axs.append(ax)
    def plot(self, label):
        field_names = self.field_name_list()
        for i in range(self.number_of_fields()):
            self.axs[i].plot(self.grid(), self.fields[i], label = label)
            self.axs[i].set_xlabel("$x$")
            self.axs[i].set_ylabel("$" + field_names[i] + "$")
            self.axs[i].legend(fontsize="x-small")
            self.figs[i].savefig(self.data_prefix + self.plot_prefix + "-" + field_names[i] + "-plot.pdf")
    def advance_timestep(self):
        self.old_fields = self.fields
        self.iteration += 1
        self.t += self.dt
    def init_cached_fields(self):
        pass

# numerics
def diff1(u, prepend, append, x):
    size = len(u)
    diff = np.zeros(size)
    for i in range(size):
        if i == 0:
            ul = prepend
            dx = (x[i+1] - x[i])
            diff[i] = (u[i] - ul)/(dx)
        elif i == 1:
            ul = u[i-1]
            ull = prepend
        else:
            ul = u[i-1]
            ull = u[i-2]
            dx = (x[i+2] - x[i])/2
            diff[i] = (ull - 4 * ul + 3 * u[i])/(2 * dx)
    return diff

def diff2(u, prepend, append, x):
    size = len(u)
    diff2 = np.zeros(size)
    for i in range(size):
        if i == 0:
            ul = prepend
        else:
            ul = u[i-1]
        if i == size - 1:
            ur = append
        else:
            ur = u[i+1]
        dx = (x[i+2] - x[i])/2
        diff2[i] = (ur - 2*u[i] + ul)/dx**2
    return diff2

def assemble_rhs(linear_implicit_equation, sol):
    return -1 * linear_implicit_equation(sol.make_zero_solution())

def assemble_matrix(linear_implicit_equation, sol, rhs):
    return np.transpose(np.vstack([linear_implicit_equation(sol.make_unit_solution(i)) + rhs for i in range(sol.size())]))

def explicit_euler_step(full_eq, sol):
    difference = u_equation(sol, sol)
    u_inner_new = sol.u_inner() + dt * difference
    sol_new = np.concatenate(([sol.u_bc_left()], u_inner_new, [sol.u_bc_right()]))
    sol.set_sol(sol_new)

def implicit_euler_step(sol_old):
    def step_func(sol_old, sol_new):
        return sol_old.full_equation(sol_new, sol_new)
    return timestep(sol_old, step_func)

def crank_nicholson_step(sol_old):
    def step_func(sol_old, sol_new):
        return sol_old.full_equation((sol_new + sol_old)/2, sol_new)
    return timestep(sol_old, step_func)

def timestep(sol_old, stepping_function):
    def implicit_function(sol_new):
        return stepping_function(sol_old, sol_new)
    b = assemble_rhs(implicit_function, sol_old)
    A = assemble_matrix(implicit_function, sol_old, b)
    sol_new = np.linalg.solve(A,b)
    return sol_old.set_sol(sol_new)

def perform_simulation(sol, timestepper=crank_nicholson_step, tolerance = 1e-15, max_iter = 200, plot_interval = 25):

    sol.max_iteration = max_iter
    sol.tolerance = tolerance
    sol.init_plotting()
    sol.plot("Iteration: " + str(sol.iteration))
    sol.plot_transformed()

    while sol.change > sol.tolerance:

        timestepper(sol)

        if np.mod(sol.iteration-1, plot_interval) == 0:
            print("iteration number: " + str(sol.iteration) + ", time: " + str(sol.t))
            print("Change: " + str(sol.change))
            sol.write_to_file(sol.data_prefix + sol.plot_prefix + "-out.h5")
            sol.plot("Iteration: " + str(sol.iteration))
            sol.plot_transformed()

        if (sol.iteration > sol.max_iteration):
            print("Maximum number of iterations reached, did not converge successfully")
            print("Residual was " + str(sol.change))
            break

    print("Done!")

    sol.plot("Final")
    sol.plot_transformed()
