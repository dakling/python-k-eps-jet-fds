#!/usr/bin/env python3

from pathlib import Path
from solution import *
# from keps_classical_jet import *

class Jet_Solution(Solution):
    mom_flux_experiment = 0.15909936
    eta_e = 0
    plot_prefix = "jet"
    data_prefix = "./data-output/"
    def ensure_mom_cons(self):
        u_mom_cons = self.u() * np.sqrt(self.mom_flux_experiment / self.mom_flux())
        self.fields[0] = u_mom_cons
    def mom_flux(self):
        J = self.number_of_nodes()
        integrand = 2 * self.u() * self.u() * self.n()
        return simpson(integrand[0:J], x = self.grid()[0:J])
    def plot_reference_solution(self):
        root_dir = str(Path(__file__).parent.absolute())
        exp_data = Jet_Solution.from_file(root_dir + "/data-input/jet-experimental.csv")
        komega_data = Jet_Solution.from_file(root_dir + "/data-input/jet-k-omega.csv")
        # keps_data = Classical_KEps_Jet_Solution.from_file(root_dir + "/data-output/classical-keps-jet-out.h5")
        self.ax_transformed.plot(exp_data.grid(), exp_data.fields[0], ".", label = r"experimental data")
        self.ax_transformed.plot(komega_data.grid(), komega_data.fields[0], ".", label = r"Wilcox k-omega data")
        # self.ax_transformed.plot(keps_data.eta(), keps_data.fields[0], ".", label = r"classical $k$-$\varepsilon$ model")
