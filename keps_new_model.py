#!/usr/bin/env python3

from keps_intermediate import *

class New_KEps_Solution(Intermediate_KEps_Solution):
    iota = 2.0

    def __init__(self, x_grid, u, k, eps, eps_hat, u_hat):
        Solution.__init__(self, x_grid, np.array([u, k, eps, eps_hat, u_hat]))
    @classmethod
    def from_file(cls, filename, iteration = None, prefix = None):
        x, fields, restart_iter, restart_time, change_vec  = cls.read_from_file(filename, iteration, prefix)
        new_inst = cls(x, fields[0], fields[1], fields[2], fields[3], fields[4])
        new_inst.iteration = restart_iter
        new_inst.t = restart_time
        new_inst.change_vec = change_vec
        return new_inst
    @classmethod
    def from_classical_model_solution(cls, classical_sol, prefix = None):
        return cls(classical_sol.grid(), classical_sol.u(), classical_sol.k(), classical_sol.eps(), classical_sol.eps(), classical_sol.u())
    def from_intermediate_model_solution(cls, intermediate_sol, prefix = None):
        return cls(intermediate_sol.grid(), intermediate_sol.u(), intermediate_sol.k(), intermediate_sol.eps(), intermediate_sol.eps_hat(), intermediate_sol.u())
    def init_plotting(self):
        super().init_plotting()
        self.multi_fig, self.multi_axs = plt.subplots(3,2,figsize = (60,30), dpi=500)
        self.fig_n, self.ax_n = plt.subplots()
        self.fig_u_terms, self.ax_u_terms = plt.subplots()
        self.fig_k_terms, self.ax_k_terms = plt.subplots()
        self.fig_eps_terms, self.ax_eps_terms = plt.subplots()
        self.fig_epshat_terms, self.ax_epshat_terms = plt.subplots()
        self.fig_u_hat_terms, self.ax_u_hat_terms = plt.subplots()
    def set_sol(self, new_sol):
        u_k_eps_epshat = new_sol.reshape(self.number_of_fields(), self.number_of_nodes())
        self.update_change(new_sol)
        self.fields = u_k_eps_epshat
        self.advance_timestep()
        self.update_cached_fields()
    def make_zero_solution(self):
        return New_KEps_Solution(self.grid(), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()))
    def make_unit_solution(self, i):
        u_k_eps_epshat = np.eye(self.size())[i].reshape(self.number_of_fields(), self.number_of_nodes())
        return New_KEps_Solution(self.grid(), u_k_eps_epshat[0], u_k_eps_epshat[1], u_k_eps_epshat[2], u_k_eps_epshat[3], u_k_eps_epshat[4])
    def u_hat(self):
        return self.fields[4]
    def u_hat_inner(self):
        return self.inner(self.u_hat())
    def u_hatl(self):
        return self.left(self.u_hat())
    def u_hatr(self):
        return self.right(self.u_hat())
    def u_hat_diff1(self):
        return self.diff1(self.u_hat())
    def u_hat_diff2(self):
        return self.diff2(self.u_hat())
    def full_equation(self, sol_intermediate, sol_new):
        return np.concatenate(([(sol_new.ul() - self.u_bc_left())], (sol_new.u_inner() - self.u_inner()) - self.dt * self.u_equation(sol_intermediate), [(sol_new.ur() - self.u_bc_right())], \
                          [(sol_new.kl() - self.k_bc_left())], (sol_new.k_inner() - self.k_inner()) - self.dt * self.k_equation(sol_intermediate), [(sol_new.kr() - self.k_bc_right())], \
                          [(sol_new.epsl() - self.eps_bc_left())], (sol_new.eps_inner() - self.eps_inner()) - self.dt * self.eps_equation(sol_intermediate), [(sol_new.epsr() - self.eps_bc_right())], \
                          [(sol_new.eps_hatl() - self.eps_hat_bc_left())], (sol_new.eps_hat_inner() - self.eps_hat_inner()) - self.dt * self.eps_hat_equation(sol_intermediate), [(sol_new.eps_hatr() - self.eps_hat_bc_right())], \
                          [(sol_new.u_hatl() - self.u_hat_bc_left())], (sol_new.u_hat_inner() - self.u_hat_inner()) - self.dt * self.u_hat_equation(sol_intermediate), [(sol_new.u_hatr() - self.u_hat_bc_right())]))
    def field_name_list(self):
        return ["u", "k", "\\varepsilon", "\\varepsilon_{hat}", "u_{hat}"]
    def plot(self, label):
        super().plot(label)
        multi_fig = self.multi_fig
        multi_axs = self.multi_axs
        field_names = self.field_name_list()
        field_index = 0
        for i in range(3):
            for j in range(2):
                if field_index < 5:
                    multi_axs[i][j].plot(self.grid(), self.fields[field_index], label = label)
                    multi_axs[i][j].set_xlabel("$x$")
                    multi_axs[i][j].set_ylabel("$" + field_names[field_index] + "$")
                    field_index += 1
        multi_axs[0][0].legend(fontsize="x-small")
        multi_fig.savefig(self.data_prefix + self.plot_prefix + "-" + "combined" + "-plot.pdf")
    def plot_transformed(self):
        field_names = self.field_name_list()
        self.ax_transformed.cla()
        self.plot_reference_solution()
        self.ax_transformed.plot(self.eta(), self.u(), label = "modified $k$-$\\varepsilon$ model")
        self.ax_transformed.set_xlim(0,0.25)
        self.ax_transformed.set_xlabel("$\\eta$")
        self.ax_transformed.set_ylabel("$" + "\\tilde{U}" + "$")
        self.ax_transformed.legend()
        self.fig_transformed.savefig(self.data_prefix + self.plot_prefix + "-plot-transformed.pdf")
