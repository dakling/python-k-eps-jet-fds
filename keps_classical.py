#!/usr/bin/env python3

from navier_stokes import *

class Classical_KEps_Solution(NS_Solution):
    c_mu = 0.09
    c_eps_1 = 1.44
    c_eps_2 = 1.92
    sigma_k = 1.00
    sigma_eps = 1.30
    k_min = 1e-10
    eps_min = 1e-10
    a_k = 0.7
    a_eps = 0.7

    def __init__(self, x_grid, u, k, eps):
        Solution.__init__(self, x_grid, np.array([u, k, eps]))
    @classmethod
    def from_file(cls, filename, iteration = None, prefix = None):
        x, fields, restart_iter, restart_time, change_vec = cls.read_from_file(filename, iteration, prefix)
        new_inst = cls(x, fields[0], fields[1], fields[2])
        new_inst.iteration = restart_iter
        new_inst.t = restart_time
        new_inst.change_vec = change_vec
        return new_inst
    def set_sol(self, new_sol):
        u_k_eps = new_sol.reshape(self.number_of_fields(), self.number_of_nodes())
        u_relaxed = self.a_u * u_k_eps[0] + (1 - self.a_u) * self.u()
        k_relaxed = self.a_k * u_k_eps[1] + (1 - self.a_k) * self.k()
        eps_relaxed = self.a_eps * u_k_eps[2] + (1 - self.a_eps) * self.eps()
        u_k_eps_relaxed = np.stack((u_relaxed, k_relaxed, eps_relaxed))
        self.advance_timestep()
        self.update_change(u_k_eps_relaxed)
        self.fields = u_k_eps_relaxed
        self.update_cached_fields()
    def update_cached_fields(self):
        self.update_n()
    def make_zero_solution(self):
        return Classical_KEps_Solution(self.grid(), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()), np.zeros(self.number_of_nodes()))
    def make_unit_solution(self, i):
        u_k_eps = np.eye(self.size())[i].reshape(self.number_of_fields(), self.number_of_nodes())
        return Classical_KEps_Solution(self.grid(), u_k_eps[0], u_k_eps[1], u_k_eps[2])
    def k(self):
        return self.fields[1]
    def eps(self):
        return self.fields[2]
    def set_fields(self, new_sol):
        self.update_change(new_sol)
        self.fields = new_sol.reshape(self.number_of_fields(), self.number_of_nodes())
    def k_inner(self):
        return self.inner(self.k())
    def kl(self):
        return self.left(self.k())
    def kr(self):
        return self.right(self.k())
    def k_diff1(self):
        return self.diff1(self.k())
    def k_diff2(self):
        return self.diff2(self.k())
    def k_lim(self):
        return np.array([max(k, self.k_min) for k in self.k()])
    def k_lim_inner(self):
        return self.inner(self.k_lim())
    def kl_lim(self):
        return self.left(self.k_lim())
    def kr_lim(self):
        return self.right(self.k_lim())
    def eps_inner(self):
        return self.inner(self.eps())
    def epsl(self):
        return self.left(self.eps())
    def epsr(self):
        return self.right(self.eps())
    def eps_diff1(self):
        return self.diff1(self.eps())
    def eps_diff2(self):
        return self.diff2(self.eps())
    def eps_lim(self):
        return np.array([max(eps, self.eps_min) for eps in self.eps()])
    def eps_lim_inner(self):
        return self.inner(self.eps_lim())
    def epsl_lim(self):
        return self.left(self.eps_lim())
    def epsr_lim(self):
        return self.right(self.eps_lim())
    def calculate_n(self):
        return self.c_mu * self.k()**2 / self.eps_lim()
    def n(self):
        return self.cached_fields[0]
    def update_n(self):
        self.cached_fields[0] = self.calculate_n()
    def full_equation(self, sol_intermediate, sol_new):
        return np.concatenate(([(sol_new.ul() - self.u_bc_left())], (sol_new.u_inner() - self.u_inner()) - self.dt * self.u_equation(sol_intermediate), [(sol_new.ur() - self.u_bc_right())], \
                          [(sol_new.kl() - self.k_bc_left())], (sol_new.k_inner() - self.k_inner()) - self.dt * self.k_equation(sol_intermediate), [(sol_new.kr() - self.k_bc_right())], \
                          [(sol_new.epsl() - self.eps_bc_left())], (sol_new.eps_inner() - self.eps_inner()) - self.dt * self.eps_equation(sol_intermediate), [(sol_new.epsr() - self.eps_bc_right())]))
    def field_name_list(self):
        return ["u", "k", "\\varepsilon"]
